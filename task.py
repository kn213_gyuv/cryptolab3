def text_to_binary(message):
    binary_message = ''
    for char in message:
        binary_char = bin(ord(char))[2:].zfill(8)
        binary_message += binary_char
    return binary_message


def xor_encrypt(message, key):
    encrypted_message = ''
    key_index = 0
    for bit in message:
        key_bit = int(key[key_index])
        encrypted_bit = int(bit) ^ key_bit
        encrypted_message += str(encrypted_bit)
        key_index = (key_index + 1) % len(key)
    return encrypted_message


def binary_to_text(binary_message):
    text = ''
    for i in range(0, len(binary_message), 8):
        byte = binary_message[i:i + 8]
        text += chr(int(byte, 2))
    return text


def main():
    message = input("Enter the message to encrypt: ")
    key = input("Enter the encryption key (binary): ")

    binary_message = text_to_binary(message)
    encrypted_message = xor_encrypt(binary_message, key)

    print("Encrypted message (binary):", encrypted_message)

    decrypted_message = xor_encrypt(encrypted_message, key)
    decrypted_text = binary_to_text(encrypted_message)
    print("Decrypted message:", decrypted_text)


if __name__ == "__main__":
    main()

